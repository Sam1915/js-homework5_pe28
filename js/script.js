function division(a, b) {
     const max = Math.max(a, b);
     const min = Math.min(a, b);
     return max / min;
}
const result = division(5, 15);
console.log(result);


function mathOp() {
     let num1 = prompt('Enter first number: ');
     while(!Number(num1) || num1 == '') {
          alert('Not a number. Try again!');
          num1 = prompt('Enter first number again: ');
     }
     let num2 = prompt('Enter second number: ');
     while(!Number(num2) || num2 == '') {
          alert('Not a number. Try again!');
          num2 = prompt('Enter second number again: ');
     }
     const max = Math.max(num1, num2);
     const min = Math.min(num1, num2);

     let operator = prompt('Chouse math operation: +, -, *, / ')
     switch (operator) {
          case '+': 
               return max + min;
               break;
          case '-': 
               return max - min;
               break;
          case '*':
               return max * min;
               break;
          case '/':
               return max / min;
               break;
     }
}
console.log(mathOp());